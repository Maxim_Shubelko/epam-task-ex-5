package model;

import lombok.*;

@Setter
@Getter
@ToString
@AllArgsConstructor
public class Plant {

    private String name;
    private float height;
}
