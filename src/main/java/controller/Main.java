package controller;

import org.xml.sax.SAXException;
import services.PlantsService;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

public class Main {
    private static final String pathInXML = "C:\\Users\\shube\\IdeaProjects\\epam-task-ex-5\\plantings.xml";
    private static final String pathOutXML = "C:\\Users\\shube\\IdeaProjects\\epam-task-ex-5\\parks.xml";
    private static final PlantsService plantsService = new PlantsService();

    public static void main(String[] args) {
        try {
            plantsService.getResultData(pathInXML, pathOutXML);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
