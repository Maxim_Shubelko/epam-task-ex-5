package utils;

import model.Plant;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class ParserPlanting {
    private static List<Plant> plantsList;

    public static List<Plant> parsePlanting(Document document) {
        plantsList = new ArrayList<>();
        Node root = document.getFirstChild();
        NodeList plantChildren = root.getChildNodes();

        for (int i = 0; i < plantChildren.getLength(); i++) {
            if (plantChildren.item(i).getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            findPlantsChildren(plantChildren.item(i).getChildNodes());
        }

        return plantsList;
    }

    public static void findPlantsChildren(NodeList plantingChild) {
        for (int i = 0; i < plantingChild.getLength(); i++) {
            if (plantingChild.item(i).getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            collectPlantsToList(plantingChild.item(i).getChildNodes());
        }

    }

    public static void collectPlantsToList(NodeList plants) {
        String name = null;
        float height = 0;

        for (int i = 0; i < plants.getLength(); i++) {
            if (plants.item(i).getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            switch (plants.item(i).getNodeName()) {
                case Constants.NAME:
                    name = plants.item(i).getTextContent();
                    break;
                case Constants.HEIGHT:
                    height = Float.parseFloat(plants.item(i).getTextContent());
                    break;
            }
        }
        plantsList.add(new Plant(name, height));
    }
}
