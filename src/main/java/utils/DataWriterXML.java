package utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class DataWriterXML {

    public static void writeData(String path, int countPlants, double totalHeight) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();
        Element rootElement = document.createElement(Constants.TAG_NAME_PARKS);
        document.appendChild(rootElement);
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StreamResult file = new StreamResult(new File(path));
        rootElement.appendChild(getPark(document, Integer.toString(countPlants), totalHeight));
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(source, file);
    }

    private static Node getPark(Document document, String countPlantsStr, double totalHeightStr) {
        Element park = document.createElement(Constants.TAG_NAME_PARK);
        park.appendChild(getParkElement(document, Constants.TAG_NAME_COUNT_PLANTS, countPlantsStr));
        park.appendChild(getParkElement(document, Constants.TAG_NAME_TOTAL_HEIGHT, String.format("%.3f", totalHeightStr)));

        return park;
    }


    private static Node getParkElement(Document doc, String tagName, String tagText) {
        Element node = doc.createElement(tagName);
        node.appendChild(doc.createTextNode(tagText));

        return node;
    }


}
