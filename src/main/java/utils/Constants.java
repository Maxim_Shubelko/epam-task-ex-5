package utils;

public abstract class Constants {
    public final static String NAME = "name";
    public final static String HEIGHT = "height";
    public final static String TAG_NAME_PARKS = "parks";
    public final static String TAG_NAME_PARK = "park";
    public final static String TAG_NAME_TOTAL_HEIGHT = "totalHeight";
    public final static String TAG_NAME_COUNT_PLANTS = "countPlants";

}
