package services;

import model.Plant;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import utils.DataWriterXML;
import utils.ParserPlanting;
import utils.ParserXML;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.List;


public class PlantsService {

    private static double calculateTotalHeight(List<Plant> plantList) {
        if (plantList == null) {
            return 0;
        }

        return plantList
                .stream()
                .mapToDouble(Plant::getHeight)
                .sum();
    }

    private static int calculateCountOfPlants(List<Plant> plantList) {
        if (plantList == null){
            return 0;
        }

        return plantList.size();
    }

    public void getResultData(String pathInXML, String pathOutXML) throws ParserConfigurationException, IOException, SAXException, TransformerException {
        Document document = ParserXML.parseXML(pathInXML);
        List<Plant> plantItems = ParserPlanting.parsePlanting(document);
        DataWriterXML.writeData(pathOutXML, calculateCountOfPlants(plantItems), calculateTotalHeight(plantItems));
    }
}
